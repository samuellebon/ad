import pika
from threading import Thread
import logging
import json
import queue

logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', level=logging.DEBUG)
logging.getLogger("pika").setLevel(logging.DEBUG)

class VideoConversionMessaging(Thread):
    def __init__(self, _config_, converting_service):
        Thread.__init__(self)
        self.credentials = pika.credentials.PlainCredentials(
                                _config_.get_rabbitmq_username(),
                                _config_.get_rabbitmq_password())
        self.connection = pika.BlockingConnection(
                            pika.ConnectionParameters(
                                _config_.get_rabbitmq_host(),
                                _config_.get_rabbitmq_port(),
                                _config_.get_rabbitmq_vhost(),
                                self.credentials))
        self.channel = self.connection.channel()
        self.rmq = _config_.get_messaging_conversion_queue()
        self.converting_service = converting_service
        self.consuming = "_CONSUMING_"
        self.rendez_vous = queue.Queue(1)
        self.pause = queue.Queue(1)
        self.start()

    def run(self):
        while True :
            if "_CONSUMING_" == self.consuming :
                method, prop, body = self.channel.basic_get(self.rmq, auto_ack=True)
                if body :
                    self._on_message_(body)
                    pass
                else :
                    try :
                        self.pause.get(timeout=1)
                    except queue.Empty :
                        pass

    def on_message(self, channel, method_frame, header_frame, body):
        logging.info(body)
        logging.info('URI = %s', body.decode())
        convert_request = json.loads(body.decode())
        logging.info(convert_request)
        self.converting_service.convert(convert_request["id"], convert_request['originPath'])

    def _on_message_(self,  body):
        logging.info(body)
        logging.info('URI = %s', body.decode())
        convert_request = json.loads(body.decode())
        logging.info(convert_request)
        self.converting_service.convert(convert_request["id"], convert_request['originPath'])

    def stop_consuming(self):
        logging.info("Stops consuming on message bus")
        self.consuming = "_IDLE_"

    def start_consuming(self):
        logging.info("Starts consuming on message bus")
        self.consuming = "_CONSUMING_"

    def is_consuming(self):
        return self.consuming
